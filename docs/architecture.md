# Architecture

## Tech Stack

- **Frontend Framework**: React with TypeScript
- **UI Library**: Material UI
- **State Management**: React Context API
- **Routing**: React Router
- **API Integration**: Axios
- **Build Tool**: Create React App

## Project Structure

```
src/
├── components/     # Reusable UI components
├── pages/         # Page components
├── services/      # API services
├── hooks/         # Custom React hooks
├── context/       # React Context providers
├── types/         # TypeScript type definitions
└── utils/         # Utility functions
```

## Component Architecture

The application follows a component-based architecture with:
- Presentational components (UI)
- Container components (Logic)
- Custom hooks for reusable logic
- Context providers for state management