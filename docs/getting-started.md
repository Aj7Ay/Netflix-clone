# Getting Started

## Prerequisites

- Node.js 16 or higher
- npm or yarn
- Git

## Installation

1. Clone the repository:
```bash
git clone https://gitlab.com/Aj7Ay/Netflix-clone.git
cd Netflix-clone
```

2. Install dependencies:
```bash
yarn install
```

3. Start the development server:
```bash
yarn start
```

The application will be available at `http://localhost:3000`.